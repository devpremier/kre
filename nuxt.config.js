import { resolve } from 'path'
import { sortRoutes } from '@nuxt/utils'

const isDev = process.env.NODE_ENV !== 'production'

export default {
  ...(!isDev && {
    modern: 'client'
  }),

  router: {
    prefetchLinks: false,
    // extendRoutes (routes) {
    //   routes.push({
    //     name: 'elitFlat',
    //     path: '/elitnie-kvartiry',
    //     meta: {
    //       filter: 'urbanFilter'
    //     },
    //     component: resolve(__dirname, 'pages/_category/index.vue')
    //   })
    //   routes.push({
    //     name: 'elitNew',
    //     path: '/elitnie-novostroyki',
    //     meta: {
    //       filter: 'urbanFilter'
    //     },
    //     component: resolve(__dirname, 'pages/_category/index.vue')
    //   })
    //   sortRoutes(routes)
    // }
  },

  rootDir: __dirname,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'front',
    htmlAttrs: {
      lang: 'ru',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  alias: {
    'images': resolve(__dirname, './assets/images'),
    'style': resolve(__dirname, './assets/style')
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'normalize.css',
    '@/assets/style/global.styl',
    '@/assets/style/grid-bootstrap.styl',
    '@/assets/style/animation.styl',
    '@/assets/style/nouislider.styl',
    '@/assets/style/slick-carousel.styl'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/vuelidate.js' },
    { src: '@/plugins/infiniteScroll.client.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
    '@aceforth/nuxt-optimized-images',
  ],

  styleResources: {
    stylus: [
      '~/assets/style/helpers/*.styl',
      '~/assets/style/mixins/*.styl'
    ],
  },

  optimizedImages: {
    optimizeImages: true
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'nuxt-webfontloader',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  webfontloader: {
    events: false,
    google: {
      families: ['Inter:300,400,500:cyrillic&display=swap']
    },
    timeout: 5000
  },

  render: {
    // http2: {
    //     push: true,
    //     pushAssets: (req, res, publicPath, preloadFiles) => preloadFiles
    //     .map(f => `<${publicPath}${f.file}>; rel=preload; as=${f.asType}`)
    //   },
    // compressor: false,
    resourceHints: false,
    etag: false,
    static: {
      etag: false
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: ['lodash'],
    optimizeCss: false,
    filenames: {
      app: ({ isDev }) => isDev ? '[name].js' : 'js/[contenthash].js',
      chunk: ({ isDev }) => isDev ? '[name].js' : 'js/[contenthash].js',
    },
    splitChunks: {
      layouts: true,
      pages: true,
      commons: true
    },
    optimization: {
      minimize: !isDev
    },
    ...(!isDev && {
      extractCSS: {
        ignoreOrder: true
      }
    }),
    postcss: {
      plugins: {
        ...(!isDev && {
          cssnano: {
            preset: ['default', {
              cssDeclarationSorter: false,
              discardComments: {
                removeAll: true
              }
            }]
          }
        })
      },
      ...(!isDev && {
        preset: {
          browsers: 'Last 2 versions',
          autoprefixer: true
        }
      }),
      order: 'cssnanoLast'
    }
  }
}
