import _orderBy from 'lodash.orderby'
import _maxBy from 'lodash.maxby'
import offersApi from '~/static/offers.json'

export const state = () => ({
  sorted: {
    ratingup: {
      sort: 'rating',
      by: 'asc'
    },
    ratingdown: {
      sort: 'rating',
      by: 'desc'
    },
    priceup: {
      sort: 'price',
      by: 'asc'
    },
    pricedown: {
      sort: 'price',
      by: 'desc'
    },
    areaup: {
      sort: 'area',
      by: 'asc'
    },
    areadown: {
      sort: 'area',
      by: 'desc'
    }
  }
})

export const actions = {
  async GET_NEW_OFFERS({ dispatch }) {
    let data = await offersApi['elitnie-kvartiry'].filter(item => item.new)

    data = await dispatch('SORT_BY', {
      data,
      sortBy: 'ratingdown'
    })

    return data.slice(0, 12)
  },
  async GET_OFFERS_DATA({ dispatch }, { category, districtId, query }) {
    let data = await offersApi[category]

    if (query?.objectWithoutPrice !== 'no') {
      data = await dispatch('FILTER_OBJECT_WITHOUT_PRICE', data)
    }

    const maxPrice = await dispatch('MAX_VALUE', { data, label: 'price' })
    const maxArea = await dispatch('MAX_VALUE', { data, label: 'area' })

    if (query?.districts) {
      data = await dispatch('FILTER_DISTRICT', {
        data,
        value: query.districts
      })
    }

    if (query?.area) {
      data = await dispatch('FILTER_AREA', { data, value: query.area })
    }

    if (query?.price) {
      data = await dispatch('FILTER_PRICE', { data, value: query.price })
    }

    if (query?.finishing) {
      data = await dispatch('FILTER_FINISHING', {
        data,
        value: query.finishing
      })
    }

    if (query?.new) {
      data = await dispatch('FILTER_NEW_OFFERS', data)
    }

    data = await dispatch('SORT_BY', {
      data,
      value: query?.sortBy || 'ratingdown'
    })

    const count = data.length

    data = await dispatch('PAGINATION', {
      data,
      currentPage: query?.currentPage || 1,
      perPage: query?.perPage || 12
    })

    return { data: { data, count, maxPrice, maxArea } }
  },
  FILTER_DISTRICT({ commit }, { data, value }) {
    return data.filter(({ district }) => {
      return value.includes(district)
    })
  },
  FILTER_AREA({ commit }, { data, value }) {
    return data.filter(i => {
      return i.area >= value[0] && i.area <= value[1]
    })
  },
  FILTER_PRICE({ commit }, { data, value }) {
    return data.filter(i => {
      return i.price >= value[0] && i.price <= value[1]
    })
  },
  FILTER_FINISHING({ commit }, { data, value }) {
    return data.filter(i => {
      return i.finishing === value
    })
  },
  FILTER_OBJECT_WITHOUT_PRICE({ commit }, data) {
    return data.filter(i => {
      return !i.hidden_price
    })
  },
  FILTER_NEW_OFFERS({ commit }, data) {
    return data.filter(i => {
      return i.new
    })
  },
  SORT_BY({ getters }, { data, value }) {
    const { sort, by } = getters.sort(value)
    if (sort === 'price') {
      return _orderBy(data, ['hidden_price', sort], ['asc', by])
    }
    return _orderBy(data, [sort], by)
  },
  PAGINATION({ commit }, { data, currentPage, perPage }) {
    const start = currentPage * perPage - perPage
    const end = start + perPage
    return data.slice(start, end)
  },
  MAX_VALUE({ commit }, { data, label }) {
    return _maxBy(data, label)[label]
  }
}

export const getters = {
  sort: s => sort => s.sorted[sort]
}
