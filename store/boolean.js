export const state = () => ({
  is_open_search: false,
  is_open_call_back: false,
  is_open_message_send: false,
  is_open_menu: false,
  is_open_login: false
})

export const mutations = {
  TOGGLE(s, payload) {
    s[payload] = !s[payload]
  }
}

export const actions = {
  TOGGLE({ commit }, nameState) {
    commit('TOGGLE', nameState)
  }
}
