import _merge from 'lodash.merge'

export const state = () => ({
  menu: {
    urban: {
      name: 'Городская',
      link: '/',
      count: null,
      parent: [
        {
          name: 'Элитные квартиры',
          link: 'elitnie-kvartiry',
          value: 'flat'
        },
        {
          name: 'Элитные новостройки',
          link: 'elitnie-novostroyki',
          value: 'flat_new'
        },
        {
          name: 'Апартаменты',
          link: 'prodazha-apartaments',
          value: 'apartments'
        },
        {
          name: 'Пентхаусы',
          link: 'prodazha-penthouse',
          value: 'penthouse'
        },
        {
          name: 'Таунхаусы',
          link: 'townhouses-moskva',
          value: 'townhouse'
        },
        {
          name: 'Квартиры в аренду',
          link: 'elitnie-kvartiry-v-arendu',
          value: 'flat_rent'
        },
        {
          name: 'Жилые комплексы',
          link: 'elitnye-zhilye-kompleksy-moskvy',
          value: 'complex'
        }
      ]
    },
    country: {
      name: 'Загородная',
      link: '/',
      count: null,
      parent: [
        {
          name: 'Загородные дома',
          link: 'zagorodnie-doma',
          value: 'village'
        },
        {
          name: 'Коттеджные поселки',
          link: 'kottedzhnye-poselki',
          value: 'cottage_villages'
        },
        {
          name: 'Аренда загородных домов',
          link: 'zagorodnie-doma-v-arendu',
          value: 'village_rent'
        },
        {
          name: 'Загородные участки',
          link: 'zagorodnie-uchastki',
          value: 'suburban_areas'
        },
        {
          name: 'Продажа таунхаусов',
          link: 'townhouses-zagorod',
          value: 'suburban_townhouse'
        },
        {
          name: 'Загородные квартиры',
          link: 'kvartiry-zagorod',
          value: 'suburban_flat'
        }
      ]
    },
    commercial: {
      name: 'Коммерческая',
      link: '/',
      count: null,
      parent: [
        {
          name: 'Коммерческая недвижимость',
          link: 'prodazha-commerce',
          value: 'com_rent'
        },
        {
          name: 'Коммерческая недвижимость в аренду',
          link: 'commerce-arenda',
          value: 'com_sell'
        }
      ]
    },
    sellers: {
      name: 'Продавцам',
      link: '/'
    },
    company: {
      name: 'Компания',
      link: '/',
      parent: [
        {
          name: 'Аналитика',
          link: '/'
        },
        {
          name: 'О компании',
          link: '/'
        },
        {
          name: 'Новости',
          link: '/'
        },
        {
          name: 'Карьера',
          link: '/'
        },
        {
          name: 'Контакты',
          link: '/'
        }
      ]
    }
  },
  titleRouteCategory: {
    'elitnie-kvartiry': 'Элитные квартиры',
    'elitnie-novostroyki': 'Элитные новоcтройки',
    'prodazha-apartaments': 'Апартаменты',
    'prodazha-penthouse': 'Элитные пентхаусы',
    'townhouses-moskva': 'Таунхаусы',
    'elitnie-kvartiry-v-arendu': 'Аренда элитных квартир',
    'zagorodnie-doma': 'Загородные дома',
    'kottedzhnye-poselki': 'Элитные коттеджные поселки',
    'zagorodnie-doma-v-arendu': 'Загородные дома в аренду',
    'zagorodnie-uchastki': 'Загородные участки',
    'townhouses-zagorod': 'Продажа таунхаусов',
    'kvartiry-zagorod': 'Загородные квартиры',
    'prodazha-commerce': 'Продажа коммерческой недвижимости в Москве',
    'commerce-arenda': 'Аренда коммерческой недвижимости в Москве'
  }
})

export const mutations = {
  addCount(s, text) {
    s.list.push({
      text,
      done: false
    })
  }
}

export const getters = {
  menu: (s, g, rootState) => {
    return _merge({}, s.menu, rootState.offers.offers)
  },
  // используется для валидации pages/_category/_district
  routeToCategory: s => Object.keys(s.titleRouteCategory),
  typeList: s => {
    const type = {}
    let i = 1
    Object.entries(s.menu).forEach(([name, value]) => {
      if (i <= 3) {
        const parent = value.parent.map(({ name, link }) => {
          return {
            label: name,
            value: link
          }
        })

        type[name] = {
          label: value.name,
          value: parent
        }
      }
      i++
    })

    return type
  }
}
