export const state = () => ({
  favorites: []
})

export const mutations = {
  GET_FAVORITES(s, payload) {
    s.favorites = payload
  },
  SAVE_FAVORITES(s, payload) {
    const indexLike = s.favorites.findIndex(item => {
      return item === payload
    })

    if (indexLike !== -1) {
      s.favorites.splice(indexLike, 1)
    } else {
      s.favorites.push(payload)
    }
  }
}

export const actions = {
  async GET_FAVORITES({ commit }) {
    const favorites = await JSON.parse(
      localStorage.getItem('favorites_kre') || '[]'
    )
    commit('GET_FAVORITES', favorites)
  },
  async SAVE_FAVORITES({ commit, state }, id) {
    commit('SAVE_FAVORITES', id)
    await localStorage.setItem('favorites_kre', JSON.stringify(state.favorites))
  }
}

export const getters = {
  favorites: s => s.favorites,
  favorites_length: s => s.favorites.length
}
