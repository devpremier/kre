import seoApi from '~/static/seo.json'

export const state = () => ({
  seoItem: {},
  titleFirst: ''
})

export const mutations = {
  SET_SEO(s, payload) {
    s.seoItem = payload
  },
  SET_TITLE(s, payload) {
    s.titleFirst = payload
  }
}

export const actions = {
  async GET_SEO({ commit }, { category, subCategory }) {
    try {
      const urlApi = subCategory || category
      const data = await seoApi.seo[urlApi]
      const title = await seoApi.seo[category]?.title

      commit('SET_SEO', data)
      commit('SET_TITLE', title)
    } catch (err) {
      console.log(err)
      throw new Error('Внутреняя ошибка сервера')
    }
  }
}

export const getters = {
  title: s => {
    return s.titleFirst !== s.seoItem?.title
      ? `${s.titleFirst} ${s.seoItem?.title}`
      : `${s.titleFirst} в Москве`
  }
}
