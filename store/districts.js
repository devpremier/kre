import districtsApi from '~/static/districts.json'

export const state = () => ({
  districts: []
})

export const mutations = {
  SET_DISTRICTS(s, payload) {
    s.districts = payload
  }
}

export const actions = {
  async GET_DISTRICTS({ commit }) {
    try {
      const data = await districtsApi.districts

      commit('SET_DISTRICTS', data)
    } catch (err) {
      console.log(err)
      throw new Error('Внутреняя ошибка сервера')
    }
  }
}

export const getters = {
  districtId: s => path => {
    return s.districts.find(item => item.path === path)?.id
  },
  districtsList: s => {
    const cao = s.districts
      .filter(i => i.affiliation === 'cao')
      .map(i => {
        return {
          value: i.id,
          label: i.name
        }
      })
    const other = s.districts
      .filter(i => i.affiliation === 'other')
      .map(i => {
        return {
          value: i.id,
          label: i.name
        }
      })

    return {
      cao: {
        label: 'Центральный АО',
        value: cao
      },
      other: {
        label: 'Другие районы',
        value: other
      }
    }
  }
}
