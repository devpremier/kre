export const state = () => ({
  offers: {
    urban: {
      count: 1000
    },
    country: {
      count: 500
    },
    commercial: {
      count: 300
    }
  },
  newOffers: [],
  offersData: [],
  type: 'elitnie-kvartiry',
  countOffers: 1,
  currentPage: 1,
  perPage: 12,
  sortBy: 'ratingdown',
  currency: 'rub',
  districts: [],
  finishing: '',
  objectWithoutPrice: 'yes',
  price: [0, 2e9],
  maxPrice: 2e9,
  area: [0, 3000],
  maxArea: 3000
})

export const mutations = {
  SET_VALUE_TO_STORE(s, { name, value }) {
    s[name] = value
  }
}

export const actions = {
  async GET_NEW_OFFERS({ commit, dispatch }) {
    try {
      const data = await dispatch('api/GET_NEW_OFFERS', null, { root: true })

      commit('SET_VALUE_TO_STORE', { name: 'newOffers', value: data })
    } catch (err) {
      console.log(err)
      throw new Error('Внутреняя ошибка сервера')
    }
  },

  async GET_OFFERS_DATA({ commit, dispatch, rootGetters }, { params, query }) {
    try {
      let dataToRequestApi = {}
      dispatch('FILTER_SET_DEFAULT', query)

      dataToRequestApi = await dispatch('PARSE_QUERY', query)

      if (params?.district) {
        // getting the district id by path
        dataToRequestApi.districts = await [
          rootGetters['districts/districtId'](params.district)
        ]

        commit('SET_VALUE_TO_STORE', {
          name: 'districts',
          value: dataToRequestApi.districts
        })
      }

      const { data } = await dispatch(
        'api/GET_OFFERS_DATA',
        {
          category: params.category,
          query: dataToRequestApi
        },
        { root: true }
      )

      commit('SET_VALUE_TO_STORE', {
        name: 'offersData',
        value: data.data
      })
      commit('SET_VALUE_TO_STORE', {
        name: 'countOffers',
        value: data.count
      })
      commit('SET_VALUE_TO_STORE', {
        name: 'maxPrice',
        value: data.maxPrice
      })
      commit('SET_VALUE_TO_STORE', {
        name: 'maxArea',
        value: data.maxArea
      })
    } catch (err) {
      console.log(err)
      throw new Error('Внутреняя ошибка сервера')
    }
  },

  async PARSE_QUERY({ commit, dispatch, state }, query) {
    const clearQuery = await dispatch('CLEAR_QUERY', query)
    const dataToRequestApi = {}

    Object.entries(clearQuery).forEach(([name, value]) => {
      if (typeof state[name] === 'object') {
        value ? (value = value.toString().split(',').map(Number)) : (value = [])
      } else if (typeof state[name] === 'number') {
        value = Number(value)
      }
      commit('SET_VALUE_TO_STORE', { name, value })
      dataToRequestApi[name] = value
    })

    return dataToRequestApi
  },

  async CHANGE_QUERY({ dispatch }, query) {
    const clearQuery = await dispatch('CLEAR_QUERY', query)
    this.$router.push({ query: clearQuery })
  },

  // clear query of null value
  CLEAR_QUERY({ commit }, query) {
    const clearQuery = {}
    Object.entries(query).forEach(([name, value]) => {
      if (value) {
        clearQuery[name] = value.toString()
      }
    })
    return clearQuery
  },

  FILTER_SET_DEFAULT({ commit }, query) {
    const filtersDefault = {
      perPage: 12,
      currentPage: 1,
      sortBy: 'ratingdown',
      currency: 'rub',
      finishing: '',
      objectWithoutPrice: 'yes',
      districts: [],
      price: [0, 2e9],
      area: [0, 3000]
    }

    Object.keys(query).forEach(n => delete filtersDefault[n])

    Object.entries(filtersDefault).forEach(([name, value]) => {
      commit('SET_VALUE_TO_STORE', { name, value })
    })
  },
  SET_VALUE_FROM_HOME_FILTER({ commit }, { name, value }) {
    commit('SET_VALUE_TO_STORE', { name, value })
  }
}
